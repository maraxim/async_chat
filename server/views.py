import asyncio
import json

import aiohttp
import aiohttp_jinja2
import asyncio_redis
from aiohttp import web

from .models import DbSlave, log


async def chat(request):
    context = {}
    response = aiohttp_jinja2.render_template('chat.html', request, context)
    return response


async def login(request):
    context = {}
    response = aiohttp_jinja2.render_template('login.html', request, context)
    return response


async def login_handler(request):
    data = await request.post()
    chat_id = data['chat_id']
    user_login = data['user_name']
    user_password = data['password']
    user_id = await DbSlave.get_user_id(user_login, user_password)
    if not user_id:
        return web.HTTPFound('/login')

    chat_id = str(chat_id)
    user_id = str(user_id)
    return web.HTTPFound('/chat/' + chat_id + '/' + user_id)


async def signin(request):
    context = {}
    response = aiohttp_jinja2.render_template('signin.html', request, context)
    return response


async def signin_handler(request):
    data = await request.post()
    user_login = data['user_name']
    user_password = data['password']
    await DbSlave.add_user(user_login, user_password)
    return web.HTTPFound('/login')


async def websocket_handler(request):
    log.info('Websocket connection starting')
    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)

    connection = await asyncio_redis.Connection.create(
        host='127.0.0.1', port=6379
    )
    connection_to_write = await asyncio_redis.Connection.create(
        host='127.0.0.1', port=6379
    )
    subscriber = await connection.start_subscribe()

    log.info('Connection opened')
    try:
        await asyncio.gather(
            from_socket_to_redis(ws, connection_to_write, subscriber),
            from_redis_to_socket(ws, subscriber),
        )
    except Exception as e:
        import traceback

        traceback.print_exc()
    finally:
        log.warn('Connection closed')
    return ws


async def from_socket_to_redis(ws, connection, subscriber):
    init_value = True
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            data = json.loads(msg.data)
            chat_id = data['chat_id']

            if init_value:
                await subscriber.subscribe([str(chat_id)])

            if msg.data == 'close':
                await ws.close()

            else:
                try:
                    message = data['message']
                    user_id = data['user']
                    await DbSlave.add_message(chat_id, user_id, message)
                except Exception:
                    pass
                data_to_client = {}

                if init_value:
                    data_to_client = await DbSlave.select_for_chat_all(chat_id)
                    init_value = False
                else:
                    data_to_client = await DbSlave.select_for_chat_last(
                        chat_id
                    )

                log.info('data_to_client' + str(data_to_client))

                await connection.publish(
                    str(chat_id), json.dumps(data_to_client)
                )
    ws.close()
    log.warn('Websocket connection closed')
    return ws


async def from_redis_to_socket(ws, subscriber):
    while True:
        msg_redis = await subscriber.next_published()
        if msg_redis:
            await ws.send_str(msg_redis.value)
