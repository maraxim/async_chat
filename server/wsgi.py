import asyncio

import aiohttp_jinja2
import jinja2
from models import DbSlave, app, log
from views import (
    chat,
    login,
    login_handler,
    signin,
    signin_handler,
    websocket_handler,
)

aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('server/templates'))

# settings server
app.router.add_get('/chat/{chat_id}/{user_id}', chat)
app.router.add_get('/login', login)
app.router.add_get('/signin', signin)
app.router.add_route('*', '/login_handler', login_handler)
app.router.add_route('*', '/singin_handler', signin_handler)
app.router.add_route('*', '/ws', websocket_handler)

# running server
loop = asyncio.get_event_loop()
handler = app.make_handler()
f = loop.create_server(handler, 'localhost', 8001)
srv = loop.run_until_complete(f)
log.info('serving on' + str(srv.sockets[0].getsockname()))

try:
    loop.run_until_complete(DbSlave.db_up())
except BaseException:
    log.critical('CANT UP DATABASE')
    app['db_engine'].close()

try:
    loop.run_forever()
except KeyboardInterrupt:
    log.info('serving off...')
finally:
    app['db'].close()
    loop.run_until_complete(handler.finish_connections(1.0))
    srv.close()
    loop.run_until_complete(srv.wait_closed())
    loop.run_until_complete(app.finish())
loop.close()
