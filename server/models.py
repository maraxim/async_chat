import datetime
import logging

import sqlalchemy as sa
from aiohttp import web
from aiopg.sa import create_engine
from sqlalchemy.schema import CreateTable

MAX_MESSAGES_GET = 20  # for initial loading chat history

app = web.Application()

logging.basicConfig(
    # filename='server.log', # uncomment for logging to file
    format=u' %(message)s [FILENAME] %(filename)s [LINE:%(lineno)d]# %(levelname)s [%(asctime)s]  ',
    level=logging.DEBUG,
)
log = logging.getLogger('ex')

users_dict = {}  # {user_id: username}

metadata = sa.MetaData()

users = sa.Table(
    'users',
    metadata,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.String(255)),
    sa.Column('password', sa.String(255)),
)

messages = sa.Table(
    'messages',
    metadata,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('user', sa.Integer, sa.ForeignKey('users.id')),
    sa.Column('chat', sa.Integer, sa.ForeignKey('chats.id')),
    sa.Column('body', sa.String(255)),
    sa.Column('date', sa.String(255)),
)

chats = sa.Table(
    'chats',
    metadata,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.String(255)),
)


class DbSlave:
    @staticmethod
    async def create_and_fill_tables(conn):
        await conn.execute('DROP TABLE IF EXISTS messages')
        await conn.execute('DROP TABLE IF EXISTS chats')
        await conn.execute('DROP TABLE IF EXISTS users')

        await conn.execute(CreateTable(users))
        await conn.execute(users.insert().values(name='maraxim', password='1'))
        await conn.execute(
            users.insert().values(name='afonasev', password='1')
        )
        await conn.execute(users.insert().values(name='kataev', password='1'))

        await conn.execute(CreateTable(chats))
        await conn.execute(chats.insert().values(name='friends'))
        await conn.execute(chats.insert().values(name='memes'))

        await conn.execute(CreateTable(messages))
        await conn.execute(
            messages.insert().values(
                body='ti molodez', user=2, chat=1, date=datetime.datetime.now()
            )
        )
        await conn.execute(
            messages.insert().values(
                body='spasibo', user=1, chat=1, date=datetime.datetime.now()
            )
        )
        await conn.execute(
            messages.insert().values(
                body='anecdot1', user=2, chat=2, date=datetime.datetime.now()
            )
        )
        await conn.execute(
            messages.insert().values(
                body='anecdot2', user=1, chat=2, date=datetime.datetime.now()
            )
        )

    @staticmethod
    async def db_up():
        app['db'] = create_engine(
            'postgresql://postgres:password@localhost:5432'
        )
        app['db_engine'] = await app[
            'db'
        ]._coro  # рискованно, но все работает в блоке try-except и в finally закрываю

        async with app['db_engine'].acquire() as conn:
            await DbSlave.create_and_fill_tables(conn)
            async for row in conn.execute(users.select()):
                users_dict[row.id] = row.name
            log.info('init user dict:' + str(users_dict))

    @staticmethod
    async def select_for_chat(chat_id, only_last):
        header = ''
        returned_arr = []

        async with app['db_engine'].acquire() as conn:

            async for row in conn.execute(
                chats.select().where(chats.c.id == str(chat_id))
            ):  # странная конструкция, но зато так можно достать только первый
                header = row.name
                break
            counter = 1

            async for row in conn.execute(
                messages.select()
                .order_by(messages.c.date.desc())
                .where(messages.c.chat == int(chat_id))
            ):

                returned_arr.append(
                    {
                        'body': row.body,
                        'date': row.date,
                        'user': users_dict[row.user],
                    }
                )
                counter += 1
                if counter > MAX_MESSAGES_GET:
                    break
                if only_last:
                    break
        message_type = 'not init' if only_last else 'init'

        return {
            'header': header,
            'message_type': message_type,
            'chat_id': chat_id,
            'data': returned_arr,
        }

    @staticmethod
    async def select_for_chat_last(chat_id):
        return await DbSlave.select_for_chat(chat_id, True)

    @staticmethod
    async def select_for_chat_all(chat_id):
        return await DbSlave.select_for_chat(chat_id, False)

    @staticmethod
    async def add_message(chat, user, body):
        date = datetime.datetime.now()
        async with app['db_engine'].acquire() as conn:
            await conn.execute(
                messages.insert().values(
                    body=body, user=user, chat=chat, date=date
                )
            )

    @staticmethod
    async def get_user_id(username, password):
        user_id = None
        async with app['db_engine'].acquire() as conn:
            async for row in conn.execute(
                users.select()
                .where(users.c.name == username)
                .where(users.c.password == password)
            ):
                user_id = row.id
        return user_id

    @staticmethod
    async def add_user(username, password):
        async with app['db_engine'].acquire() as conn:
            await conn.execute(
                users.insert().values(name=username, password=password)
            )
            async for row in conn.execute(
                users.select().order_by(users.c.id.desc())
            ):
                users_dict[row.id] = row.name
                break
