# Async chat

Async chat. Using asyncio, aiohttp in core.

Also using sqlalchemy, aiopg, websockets, asyncio_redis(for Redis Pub/Sub) 

# Using
Server runs on 8001 port.

For login-ing just using localstorage and site's url.

Server has not protected from fool :D. I mean, you can just type /chat/1/2 and send to chat#1 from user#2 

## Api
    /login 
    /singin
    /chat/chat_id/user_id


## Init project

    make init precommit_install

## Run dev server
    make up

### With database  
    make up-database
    make stop-database
    make rm-database
    
## Run linters, autoformat and tests
    make pretty lint test

## P.S 
    Maybe I lost some depends )
    For resolve u need: 
    cd .venv/bin 
    pip3 install needs_package

## P.S #2
    Troubles with path 
    Need for run server: from models import DbSlave, log
    But for tests: from .models import DbSlave, log 


