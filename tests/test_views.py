import aiohttp_jinja2
import jinja2
import pytest
from aiohttp import web

from server.views import chat, login, signin


@pytest.fixture()
def app_for_test():
    app = web.Application()
    aiohttp_jinja2.setup(
        app, loader=jinja2.FileSystemLoader('server/templates')
    )
    return app


async def test_login2(aiohttp_client, loop, app_for_test):
    app_for_test.router.add_get('/', login)
    client = await aiohttp_client(app_for_test)
    resp = await client.get('/')
    assert resp.status == 200
    text = await resp.text()
    assert '<h1 > Log in </h1>' in text


async def test_signin(aiohttp_client, loop, app_for_test):
    app_for_test.router.add_get('/', signin)
    client = await aiohttp_client(app_for_test)
    resp = await client.get('/')
    assert resp.status == 200
    text = await resp.text()
    assert '<h1> Sign in </h1>' in text


async def test_chat(aiohttp_client, loop, app_for_test):
    app_for_test.router.add_get('/chat/1/1', chat)
    client = await aiohttp_client(app_for_test)
    resp = await client.get('/chat/1/1')
    assert resp.status == 200
    text = await resp.text()
    assert '<input type=text id="message_body" >' in text
