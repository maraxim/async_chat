import pytest

from server.models import DbSlave


@pytest.mark.asyncio
async def test_select_for_chat():
    await DbSlave.db_up()
    chat_id = 1
    init_messages = await DbSlave.select_for_chat_all(chat_id)
    assert init_messages['message_type'] == 'init'
    assert init_messages['chat_id'] == chat_id
    assert init_messages['header'] != ''


@pytest.mark.asyncio
async def test_select_for_chat_last():
    await DbSlave.db_up()
    chat_id = 1
    init_messages = await DbSlave.select_for_chat_last(chat_id)
    assert init_messages['message_type'] != 'init'
    assert init_messages['chat_id'] == chat_id
    assert init_messages['header'] != ''
    assert len(init_messages['data']) == 1


@pytest.mark.asyncio
async def test_add_message():
    await DbSlave.db_up()
    chat_id = 1
    user_id = 1
    await DbSlave.add_message(chat_id, user_id, 'test_message')
    last_message = await DbSlave.select_for_chat_last(chat_id)

    assert last_message['message_type'] != 'init'
    assert last_message['chat_id'] == chat_id
    assert last_message['header'] != ''
    assert len(last_message['data']) == 1
    assert last_message['data'][0]['body'] == 'test_message'


@pytest.mark.asyncio
async def test_get_user_id():
    await DbSlave.db_up()
    user = 'maraxim'
    password = '1'
    user_id = await DbSlave.get_user_id(user, password)
    assert user_id == 1  # sets in DbSlave.db_up()


@pytest.mark.asyncio
async def test_add_user():
    await DbSlave.db_up()
    user = 'maraxim'
    password = '1'
    await DbSlave.add_user(user, password)
    user_id = await DbSlave.get_user_id(user, password)
    assert user_id == 4  # 3 users sets in DbSlave.db_up()
